package com.example.limo2.roamramadantest;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by limo2 on 11/29/17.
 */
@Dao
public interface ConDao {

//    @Query("SELECT * FROM BaseContent")
//    Single< List<BaseContent>>  getAllCoupn();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCoupn(BaseContent baseContent);


    @Query("SELECT * FROM BaseContent")
    Flowable<List<BaseContent>> getCouponsFlowable();

    @Delete
    void deleteItem(BaseContent item);


    @Query("DELETE FROM BaseContent WHERE coupId= :id")
    void deleteById(int id);
    /*
    @Insert
    void insertItem(TranslateItem item);

    @Update
    void setUnsetFavourite(TranslateItem item);

    @Delete
    void deleteItem(TranslateItem item);

    @Query("SELECT * FROM TranslateItem")
    Single<List<TranslateItem>> getAllItems();

    @Query("SELECT * FROM TranslateItem WHERE wordIn LIKE :word")
    Maybe<TranslateItem> checkItem(String word);

    @Query("SELECT * FROM TranslateItem WHERE wordIn LIKE :word OR wordOut LIKE :word")
    Single<List<TranslateItem>> searchLocal(String word);

    @Query("SELECT * FROM TranslateItem WHERE isFavorite = 1 AND (wordIn LIKE :word OR wordOut LIKE :word)")
    Single<List<TranslateItem>> searchLocalFavourite(String word);

//    @Query("SELECT * FROM TranslateItem WHERE isFavorite LIKE :1")
//    List<TranslateItem> getAllFavorite();


     */
}
