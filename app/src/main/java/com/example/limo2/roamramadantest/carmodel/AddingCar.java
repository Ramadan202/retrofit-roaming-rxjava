package com.example.limo2.roamramadantest.carmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramadan on 11/22/2017.
 */

public class AddingCar {

    @SerializedName("clientId")
    private int clientId;
    @SerializedName("brandId")
    private int brandId;
    @SerializedName("modelId")
    private int modelId;
    @SerializedName("color")
    private String color;
    @SerializedName("km")
    private int km;
    @SerializedName("painting")
    private String painting;
    @SerializedName("year")
    private int year;
    @SerializedName("note")
    private String note;

    public AddingCar() {
    }

    public int getClientId() {
        return clientId;
    }

    public int getBrandId() {
        return brandId;
    }

    public int getModelId() {
        return modelId;
    }

    public String getColor() {
        return color;
    }

    public int getKm() {
        return km;
    }

    public String getPainting() {
        return painting;
    }

    public int getYear() {
        return year;
    }

    public String getNote() {
        return note;
    }
}
