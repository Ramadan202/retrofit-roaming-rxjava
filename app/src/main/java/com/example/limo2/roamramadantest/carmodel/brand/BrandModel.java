package com.example.limo2.roamramadantest.carmodel.brand;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramadan on 11/14/2017.
 */

public class BrandModel {

    @SerializedName("id")
    private int  modelId;
    @SerializedName("modelName")
    private String modelName;


    public BrandModel() {
    }

    public int getModelId() {
        return modelId;
    }

    public String getModelName() {
        return modelName;
    }
}
