package com.example.limo2.roamramadantest;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by limo2 on 11/27/17.
 */

public interface CouponService {

    @FormUrlEncoded
    @POST("/TripU.asmx/GetCouponinfo")
    Call<BaseModel> getCouponDat(@Field("couponCode") String couponCode, @Field("clientId") int clientId, @Field("CountryCode") String CountryCode);

    @FormUrlEncoded
    @POST("/TripU.asmx/GetCouponinfo")
    Flowable<BaseModel> getCouponInfoData(@Field("couponCode") String couponCode, @Field("clientId") int clientId, @Field("CountryCode") String CountryCode);



    @FormUrlEncoded
    @POST("/TripU.asmx/GetCouponinfo")
    Observable<BaseModel> getCouponInfoDa(@Field("couponCode") String couponCode, @Field("clientId") int clientId, @Field("CountryCode") String CountryCode);



}
