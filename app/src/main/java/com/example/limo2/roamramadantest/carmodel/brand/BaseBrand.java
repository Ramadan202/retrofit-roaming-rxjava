package com.example.limo2.roamramadantest.carmodel.brand;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ramadan on 11/14/2017.
 */

public class BaseBrand {


    @SerializedName("statusCode")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("content")
    List<BrandContent> brandContentList;

    public BaseBrand() {
    }


    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<BrandContent> getBrandContentList() {
        return brandContentList;
    }
}
