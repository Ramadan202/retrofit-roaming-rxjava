package com.example.limo2.roamramadantest;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by limo2 on 11/27/17.
 */

@Entity
public class BaseContent  {

    @SerializedName("coupId")
    @PrimaryKey
    private int coupId;

    @SerializedName("coupValue")
    private int coupValue;

    @SerializedName("percentage")
    @ColumnInfo(name = "Per%")
    private int  percentage;

    @Ignore
    public BaseContent() {
    }

    public BaseContent(int coupId, int coupValue, int percentage) {
        this.coupId = coupId;
        this.coupValue = coupValue;
        this.percentage = percentage;
    }

    public void setCoupId(int coupId) {
        this.coupId = coupId;
    }

    public void setCoupValue(int coupValue) {
        this.coupValue = coupValue;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public int getCoupId() {
        return coupId;
    }

    public int getCoupValue() {
        return coupValue;
    }

    public int getPercentage() {
        return percentage;
    }

    //@Entity(tableName = "desserts")
    //@PrimaryKey(autoGenerate = true)

}
