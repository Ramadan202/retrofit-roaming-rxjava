package com.example.limo2.roamramadantest;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.limo2.roamramadantest.carmodel.brand.BaseBrand;
import com.example.limo2.roamramadantest.carmodel.brand.BrandContent;
import com.example.limo2.roamramadantest.carmodel.brand.BrandModel;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity{
    boolean x=false;
    AppDatabase database;
    ConDao conDao;
    private CompositeDisposable compositeDisposable;
    private CarService carService;
    List<BrandContent> brandsSpnnerList;
    List<BrandModel> brandModels;
    List<String> stringList;
    int positionOfBrand=0;
    int poaitionOfModel=0;
    Spinner brandsSp;
    Spinner modelsp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String x="123456";
        int y=Integer.parseInt(x);
        int z=Integer.valueOf(x);

        Toast.makeText(this, "y"+y, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "z"+z, Toast.LENGTH_SHORT).show();
        carService = ApiModule.provideRetrofit(MainActivity.this).create(CarService.class);
         brandsSp =  findViewById(R.id.brand_spinner_id);
        modelsp =  findViewById(R.id.model_spinner_id);
        brandsSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                positionOfBrand=position;
                setDatatoModel(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        modelsp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poaitionOfModel=position;
                showModelId(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getBrandModel();




        compositeDisposable=new CompositeDisposable();

        //getDtaCall();
        database = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "limozein_b").build();

        conDao=database.conDao();


        Button insert = findViewById(R.id.insert_dessert_btn);
        insert.setOnClickListener((View view) -> {


//            Completable.<Boolean>create(completableSubscriber -> {
//                BaseContent baseContent = new BaseContent();
//                baseContent.setCoupId(1);
//                baseContent.setCoupValue(2);
//                baseContent.setPercentage(3);
//                conDao.insertCoupn(baseContent);
//                completableSubscriber.onComplete();
//
//
//            }).subscribeOn(io.reactivex.schedulers.Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new CompletableObserver() {
//                        @Override
//                        public void onSubscribe(Disposable d) {
//
//                            Log.v("insertdata", "finshed");
//                        }
//
//                        @Override
//                        public void onComplete() {
//
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//                    });



//            Flowable<Boolean> booleanFlowable=Flowable.fromCallable(new Callable<Boolean>() {
//                @Override
//                public Boolean call() throws Exception {
//                    BaseContent baseContent = new BaseContent();
//                    baseContent.setCoupId(2);
//                    baseContent.setCoupValue(3);
//                    baseContent.setPercentage(4);
//                    conDao.insertCoupn(baseContent);
//                    x= true;
//                    return x;
//                }
//            });
//
//            Disposable disposablettt=booleanFlowable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
//                    subscribe(new Consumer<Boolean>() {
//                        @Override
//                        public void accept(Boolean aBoolean) throws Exception {
//                            if(aBoolean)
//                            {
//                                Log.v("insertdata", "finshed");
//                            }
//                        }
//                    });
//
//            compositeDisposable.add(disposablettt);
            saveData(new ValueCallback<Boolean>() {
                @Override
                public void onReceiveValue(Boolean value) {
                    if(value)
                    {
                        Log.v("insertdata", "finshed");
                    }
                }
            });

        });





        Button delete = findViewById(R.id.delete_dessert_btn);
        delete.setOnClickListener(viewtwo -> {

      //      conDao.deleteById(1);

            Completable completable=  Completable.fromAction(() -> {
//                    BaseContent baseContent=new BaseContent();
//                    baseContent.setCoupId(1);
//                    baseContent.setCoupValue(2);
//                    baseContent.setPercentage(3);
//                    conDao.deleteItem(baseContent);
                conDao.deleteById(1);
            });

            Disposable disposable= completable.subscribeOn(io.reactivex.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                    subscribe(new Action() {
                        @Override
                        public void run() throws Exception {
                            Log.v("DEL","DELETEdONE");
                        }
                    });




        });



        Button show = findViewById(R.id.showItems);

        show.setOnClickListener(viewclick ->


        {

            Flowable<List<BaseContent>> listFlowable=conDao.getCouponsFlowable();
            Disposable disposabletwo=listFlowable.onBackpressureBuffer().subscribeOn(io.reactivex.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                    subscribe(baseContents -> {
                        Log.v("sizelist",baseContents.get(0).getCoupValue()+"");
                    });


        });








//
//
//        ApiModule.provideApiService().getCouponInfoData("limo",1,"EG").subscribeOn(Schedulers.io()).
//                observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<BaseModel>() {
//
//            @Override
//            public void onSubscribe(Disposable d) {
//
//            }
//
//            @Override
//            public void onNext(BaseModel baseModel) {
//
//                Toast.makeText(MainActivity.this, "From rx"+baseModel.getBaseContent().getCoupId(), Toast.LENGTH_SHORT).show();
//
//                Log.v("message",baseModel.getMessage());
//
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//                Toast.makeText(MainActivity.this, ""+e, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });




//
//        Flowable<BaseModel> baseModelFlowable= ApiModule.provideApiService().getCouponInfoData("limo",1,"EG");
//
//       compositeDisposable.add(baseModelFlowable.onBackpressureBuffer().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
//                subscribe(baseModel -> {
//                    Toast.makeText(this, ""+baseModel.getStatus(), Toast.LENGTH_SHORT).show();
//                }));
//
//
////
//        Observable<List<String>> baseModelObservable = Observable.fromCallable(() -> getColorList());
//
//
//
//
//        Disposable disposable=baseModelObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
//                subscribe(strings -> {
//
//                    Toast.makeText(this, ""+strings.size(), Toast.LENGTH_SHORT).show();
//                });
//
//        compositeDisposable.add(disposable);


    }
    @Override
    public void onStop() {
        super.onStop();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
            compositeDisposable.clear();
        }

    }
    private  List<String> getColorList() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("red");
        colors.add("green");
        colors.add("blue");
        colors.add("pink");
        colors.add("brown");
        return colors;
    }

    public void saveData(ValueCallback<Boolean> callback)
    {

        Flowable<Boolean> booleanFlowable=Flowable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                BaseContent baseContent = new BaseContent();
                baseContent.setCoupId(2);
                baseContent.setCoupValue(3);
                baseContent.setPercentage(4);
                conDao.insertCoupn(baseContent);
                x= true;
                return x;
            }
        });

        Disposable disposablettt=booleanFlowable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if(aBoolean)
                        {
                            callback.onReceiveValue(aBoolean);
                        }
                        else
                        {
                            callback.onReceiveValue(false);
                        }
                    }
                });

        compositeDisposable.add(disposablettt);

    }

    public void getData()
    {
        CouponService couponService= ApiModule.provideRetrofit(MainActivity.this).create(CouponService.class);

        Flowable<BaseModel> baseModelFlowable=couponService.getCouponInfoData("limo",1,"EG").onBackpressureBuffer();

        Disposable disposable=baseModelFlowable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<BaseModel>() {
            @Override
            public void accept(BaseModel baseModel) throws Exception {

                Toast.makeText(MainActivity.this, ""+baseModel.getBaseContent().getPercentage(), Toast.LENGTH_SHORT).show();
            }
        });
        compositeDisposable.add(disposable);

        //  ApiModule.provideApiService().getCouponInfoData("limo",1,"EG");
//
//       compositeDisposable.add(baseModelFlowable.onBackpressureBuffer().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
//                subscribe(baseModel -> {
//                    Toast.makeText(this, ""+baseModel.getStatus(), Toast.LENGTH_SHORT).show();
//                }));
    }

    public void getDtaCall()
    {
        CouponService couponService= ApiModule.provideRetrofit(MainActivity.this).create(CouponService.class);

        couponService.getCouponDat("limo",1,"EG").enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {

                Toast.makeText(MainActivity.this, ""+response.body().getStatus(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {

            }
        });

    }

    public void getBrandModel()
    {
        brandsSpnnerList = new ArrayList<>();

        carService.getAllbrandAndModels().enqueue(new Callback<BaseBrand>() {
            @Override
            public void onResponse(Call<BaseBrand> call, retrofit2.Response<BaseBrand> response) {

                if(response.body().getStatus()== 200)
                {
                    brandsSpnnerList.addAll(response.body().getBrandContentList());

                    CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), brandsSpnnerList);
                    brandsSp.setAdapter(customAdapter);
                }
            }

            @Override
            public void onFailure(Call<BaseBrand> call, Throwable t) {

            }
        });

    }
//
//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//        switch (view.getId())
//        {
//            case R.id.brand_spinner_id:
//                setDatatoModel(position);
//                positionOfBrand=position;
//                Toast.makeText(this, ""+positionOfBrand, Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.model_spinner_id:
//                poaitionOfModel=position;
//                Toast.makeText(this, ""+poaitionOfModel, Toast.LENGTH_SHORT).show();
//
//                Toast.makeText(this, ""+brandsSpnnerList.get(positionOfBrand).getBrandModelList().get(poaitionOfModel).getModelName(), Toast.LENGTH_SHORT).show();
//                break;
//        }
//
//    }



    void setDatatoModel(int position)
    {

        brandModels=new ArrayList<>();
        stringList=new ArrayList<>();
        brandModels=brandsSpnnerList.get(position).getBrandModelList();
        for (int x=0;x<brandModels.size();x++)
        {
            stringList.add(brandModels.get(x).getModelName());
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_item,stringList );

        modelsp.setAdapter(spinnerArrayAdapter);
    }

    void showModelId(int position)
    {

        showToast();

    }

    void showToast()
    {
        Toast.makeText(this, ""+brandsSpnnerList.get(positionOfBrand).getBrandId(), Toast.LENGTH_SHORT).show();

        Toast.makeText(this, ""+brandModels.get(poaitionOfModel).getModelId(), Toast.LENGTH_SHORT).show();

    }
}
