package com.example.limo2.roamramadantest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.limo2.roamramadantest.carmodel.brand.BrandContent;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by siko on 8/3/2017.
 */

public class CustomSpinnerAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    List<BrandContent> brandModelList;
    public CustomSpinnerAdapter(Context applicationContext, List<BrandContent> brands) {
        this.context = applicationContext;
        this.brandModelList = brands;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return brandModelList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);
        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        TextView brandName = (TextView) view.findViewById(R.id.textView);
        String imageurl= brandModelList.get(position).getBrandLogo();
        if(!imageurl.isEmpty())
        {

            Picasso picasso =ApiModule.cachImageUsingPicaso(context);
            picasso.load(imageurl).networkPolicy(NetworkPolicy.OFFLINE,NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE).noFade().into(icon);


//            Glide.with(context).load(imageurl).diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(icon);

        }
        brandName.setText(brandModelList.get(position).getBrandName());

        return view;
    }
}