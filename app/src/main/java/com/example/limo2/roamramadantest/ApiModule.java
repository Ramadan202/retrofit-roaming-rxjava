package com.example.limo2.roamramadantest;


import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class ApiModule {

    public static String mBaseUrl ="https://limoapi.azurewebsites.net";


    public static Retrofit provideRetrofit(Context context) {

        OkHttpClient okHttpClient=okHttpClient(loggingInterceptor(),cache(cacheFile(context)));
        Gson gson=  provideGson();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    public static HttpLoggingInterceptor loggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.i(message);
            }
        });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return interceptor;
    }


    public static Cache cache(File cacheFile) {
        return new Cache(cacheFile, 10 * 1000 * 1000); //10MB Cahe
    }


    public static File cacheFile(Context context) {
        return new File(context.getCacheDir(), "okhttp_cache");
    }


    public static OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor, Cache cache) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .cache(cache)
                .build();
    }

    public static Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    public static Picasso cachImageUsingPicaso(Context context)
    {
        OkHttpClient okHttpClient=okHttpClient(loggingInterceptor(),cache(cacheFile(context)));
        OkHttp3Downloader okHttpDownloader = new OkHttp3Downloader(okHttpClient);
        Picasso picasso = new Picasso.Builder(context).downloader(okHttpDownloader).build();
        return picasso;

    }

//    public final static  String BASE_URL = "https://limoapi.azurewebsites.net";
//
//
//    public static OkHttpClient provideClient() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//    }
//
//
//    public static Retrofit provideRetrofit(String baseURL, OkHttpClient client) {
//        return new Retrofit.Builder()
//                .baseUrl(baseURL)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .build();
//    }
//
//    public static CouponService provideApiService() {
//        return provideRetrofit(BASE_URL, provideClient()).create(CouponService.class);
//    }
//
//
//    public static CouponService SetBaseUrlAndGetRetrofit()
//    {
//        Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
//        CouponService limozeinAPI=retrofit.create(CouponService.class);
//        return limozeinAPI;
//
//    }//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

}
