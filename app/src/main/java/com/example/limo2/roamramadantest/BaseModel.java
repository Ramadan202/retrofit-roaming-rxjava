package com.example.limo2.roamramadantest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by limo2 on 11/27/17.
 */

public class BaseModel  {

    @SerializedName("statusCode")
    private int status;
    @SerializedName("message")

    private String message;
    @SerializedName("content")

    BaseContent baseContent;

    public BaseModel() {
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public BaseContent getBaseContent() {
        return baseContent;
    }
}
