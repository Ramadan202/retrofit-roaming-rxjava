package com.example.limo2.roamramadantest;

import com.example.limo2.roamramadantest.carmodel.brand.BaseBrand;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ramadan on 11/22/2017.
 */

public interface CarService {

    @POST("/ClientCar.asmx/GetbrandAndModel")
    Call<BaseBrand> getAllbrandAndModels();


}
