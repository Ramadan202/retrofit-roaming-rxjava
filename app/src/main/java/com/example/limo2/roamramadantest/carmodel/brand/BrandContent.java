package com.example.limo2.roamramadantest.carmodel.brand;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ramadan on 11/14/2017.
 */

public class BrandContent {

    @SerializedName("id")
    private int  brandId;
    @SerializedName("name")
    private String brandName;
    @SerializedName("logo")
    private String brandLogo;
    @SerializedName("model")
    List<BrandModel> brandModelList;

    public BrandContent() {
    }

    public int getBrandId() {
        return brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public List<BrandModel> getBrandModelList() {
        return brandModelList;
    }
}
